library(crsuggest)
library(arrow)
library(geoarrow)
library(dplyr)

tile <- geoarrow_collect_sf(filter(open_dataset("inst/parquet/sentinel2_grid.parquet"), Name %in% c("48PVT", "31TCJ")))
tile

tile$epsg_code <- sapply(1:nrow(tile), function(i) {
  suggested_crs_data <- suggest_crs(tile[i, 1])
  numeric_result <- as.numeric(suggested_crs_data[grepl("^WGS 84", suggested_crs_data$crs_name) & grepl("^32", suggested_crs_data$crs_code), "crs_code"])
  if (length(numeric_result) > 0) numeric_result[1] else NA
})
tile
