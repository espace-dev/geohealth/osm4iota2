---
output: github_document
---

<!-- README.md is generated from README.Rmd. Please edit that file -->

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>",
  fig.path = "man/figures/README-",
  out.width = "100%"
)
```

# osm4iota2

<!-- badges: start -->

<!-- badges: end -->

## Package dependencies

```{r echo=FALSE, message=FALSE, warning=FALSE}
quiet <- function(x) { 
  sink(tempfile()) 
  on.exit(sink()) 
  invisible(force(x)) 
} 
suppressPackageStartupMessages(library(pkgndep))
pkg <- quiet(pkgndep(getwd(), verbose = FALSE))
dependency_heatmap(pkg, file = "man/figures/dependency_heatmap.png")
```

![](man/figures/dependency_heatmap.png)
